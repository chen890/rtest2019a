setwd("D:/angular/rtest2019a")

#Q1.1
student<-read.csv('data.csv')
str(student)
summary(student)
student.cp<-read.csv('data.csv')
summary(student.cp)


student$id<-NULL
student$failures<-as.factor(student$failures)
student$studytime<-as.factor(student$studytime)
student$goout<-as.factor(student$goout)
student$address<-as.factor(student$address)
student$Medu<-as.factor(student$Medu)
student$Fedu<-as.factor(student$Fedu)
student$traveltime<-as.factor(student$traveltime)
str(student)


#Q1.2
NaFinder<-function(x)
{
  vecFalseOrTrue<-apply(X = x, MARGIN = 2,function(x) any(is.na(x)))
  return (vecFalseOrTrue)
}
#Give TRUE where in a col there is NA's.
NaFinder(student)
#there is no missing values in the dataset



#Q1.3
library(ggplot2)
ggplot(student,aes(G3, fill=higher))+geom_bar()
#there is unusual students with higer=yes and still didnt make it in G3
# if we want to remove them from the data:
noZeroG3<-student.cp$G3==0
student.cp<-student[!noZeroG3,]
ggplot(student.cp,aes(G3, fill=higher))+geom_bar()




#Q2.1
str(student)
levels(x = student$school)
ggplot(student,aes(G3, fill=school))+geom_bar()
#we can see that students from GP more likely to get a better final grade.
ggplot(student.cp,aes(G3, fill=school))+geom_bar()


#Q2.2
hist(student$absences,col = 'blue',breaks = 50)
ggplot(student,aes(absences))+geom_histogram(binwidth = 1)
# Yes, there is some unusual student how more absences than others.

#Remove unusual student from absences
absencesmore30<-student.cp$absences>30
student.cp<-student.cp[!absencesmore30,]
ggplot(student.cp,aes(absences))+geom_histogram(binwidth = 1)





#Q2.3
student$Above11<-student$G3>11
student$Above11<-as.factor(student$Above11)
str(student)
student.cp$Above11<-student.cp$G3>11
student.cp$Above11<-as.factor(student.cp$Above11)
str(student.cp)
ggplot(student.cp,aes(absences))+geom_histogram(binwidth = 1)
cor(student.cp$absences,student.cp$Above11)
# cor is (-0.161): low corrilation between them






#Q2.4
ggplot(student,aes(G3, fill=failures))+geom_bar()
ggplot(student,aes(G3, fill=failures))+geom_bar(position='fill')
#Yes, more than you fail, the higher chance  that your G3 will be lower
#there is unusual student with 0 fails and still they're G3 is 0





#Q3.1
library(caTools) ## Linear Rigresion Model
str(student)
filterLR <- sample.split(student, SplitRatio = 0.7)
studentLR.train <- subset(student, filterLR ==T)
studentLR.test <- subset(student, filterLR ==F)
dim(studentLR.test)
dim(studentLR.train)

#Q3.2
modelLR<-lm(G3~. , studentLR.test)
summary(modelLR)
sm<-summary(modelLR)
mse <- function(sm)
{
   mean(sm$residuals^2)
} 
#MSE for the model LR
mse(modelLR)

ggplot(student,aes(G3,Above11))+geom_point()
ggplot(student.test,aes(G3,Above11))+geom_point()
prediciton <- studentLR.test$G3 > 11 ## decision boundary
actual <- studentLR.test$Above11
cf <- table(prediciton,actual)
precision <-cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['TRUE','FALSE'] )
recall <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['FALSE','TRUE'] )
# the model is giving no overfitting and no underfitting
# we can know that by cf and see that every prediciton TRUE is actual TRUE.



#Q3.3
str(student)
student$equalormore11<-student$G3>=11
student$equalormore11<-as.factor(student$equalormore11)
filter <- sample.split(student, SplitRatio = 0.7)
student.train <- subset(student, filter ==T)
student.test <- subset(student, filter ==F)

predict.prob <- predict(modelLR,student.test)
prediciton <- predict.prob.yes > 11 ## decision boundary
actual <- studentLR.test$G3
cf <- table(prediciton,actual)
precision <-cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['TRUE','FALSE'] )
recall <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['FALSE','TRUE'] )
total_errors <- (cf['TRUE','FALSE']+cf['FALSE','TRUE'])/dim(df.test)[1]

numofequalormore11<-table(student$equalormore11)["TRUE"]
numofnoequalormore11<-table(student$equalormore11)["FALSE"]
pre<-numofnoequalormore11/(numofequalormore11+numofnoequalormore11)
pre
# 47.1% will be fail.







#Q4.1
maxG3<-max(... = student$G3)
topStudentScore<-maxG3*(0.8)
str(student)
student$Above18<-student$G3>=topStudentScore
student$Above18<-as.factor(student$Above18)
str(student)


#Q4.2
install.packages('e1071')
library(e1071) ## Naive Base Model
filterNB <- sample.split(student$Above18 , SplitRatio = 0.7)
studentNB.train <- subset(student, filterNB ==T)
studentNB.test <- subset(student, filterNB ==F)
modelNB <- naiveBayes(Above18~., studentNB.train)
prediction <- predict(modelNB,studentNB.test, type = "raw")
predict <- prediction[,'TRUE']
actual <- studentNB.test$Above18
predicted <- predict >0.94 ## decision boundary
cfNB <- table(predicted, actual)
precision <-cfNB['TRUE','TRUE']/(cfNB['TRUE','TRUE'] + cfNB['TRUE','FALSE'] )
recall <- cfNB['TRUE','TRUE']/(cfNB['TRUE','TRUE'] + cfNB['FALSE','TRUE'] )



#Q4.3
install.packages('rpart')
install.packages('rpart.plot')
library(rpart) ## Random Forest
library(rpart.plot) ## Show Random Forest
modelRF<-rpart(Above18~.,studentNB.train) 
rpart.plot(modelRF,box.palette = 'RdBu', shadow.col = "grey", nn = TRUE)
## the result in the plot is the probability of each row.


## Errors model From Random Forest
#calculating the confusion matrix
predict.prob <- predict(modelRF,studentNB.test)
predict.prob.yes <- predict.prob[,'TRUE']
prediciton <- predict.prob.yes > 0.5 ## decision boundary
actual <- studentNB.test$Above18
cfRF <- table(prediciton,actual)
precision <-cfRF['TRUE','TRUE']/(cfRF['TRUE','TRUE'] + cfRF['TRUE','FALSE'] )
recall <- cfRF['TRUE','TRUE']/(cfRF['TRUE','TRUE'] + cfRF['FALSE','TRUE'] )
total_errors <- (cfRF['TRUE','FALSE']+cfRF['FALSE','TRUE'])/dim(studentNB.test)[1]



#Q4.4
#ROC curve 
install.packages('pROC')
library(pROC)
rocCurveNB <- roc(studentNB.test$Above18, predict.prob.yes, direction = "<", levels = c("FALSE","TRUE"))
rocCurveRF <- roc(studentNB.test$Above11, predict, direction = "<", levels = c("FALSE","TRUE"))
#Calculate AUC
auc(rocCurveRF)
auc(rocCurveNB)
    #plot of ROC
plot(rocCurveRF, col="red", main='ROC chart')
par(new=TRUE)
plot(rocCurveNB, col="blue", main='ROC chart')


# the rocCurveRF is more likely to be realistic by common sense, but it doesn't say the rocCurveNB in not currect.
#both of the ROC curevs are good -the NB model and the RF model have almost the same results.